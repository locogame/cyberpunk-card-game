// file: src/GameLogic.js
import CardPrototypes from './CardPrototypes.json';
import {GameHelper, ImmutableArray} from './GameHelper';
import {GameEvents, GameEventValidator} from './GameEvents';

function initialState(ctx, state) {
    let cardId = 0;
    let cards = [];
    for (let duplicate = 0; duplicate < 4; duplicate++) {
        for (let index = 0; index < CardPrototypes.length; index++) {
            cards.push({
                id: cardId++,
                proto: CardPrototypes[index]
            });
        }
    }
    let initialState = state || {
        player_0: {
            deck: [0, 1, 2, 4, 5, 6],
            hand: [7, 8],
            field: [3],
            trash: [],
            maxCpu: 1,
            cpu: 0,
            memory: 4
        },
        player_1: {
            deck: [9, 10, 11],
            hand: [],
            field: [12, 19],
            trash: [],
            maxCpu: 1,
            cpu: 0,
            memory: 4
        },
        cards
    };
    const zones = ['deck', 'hand', 'field', 'trash'];
    const players = ['player_0', 'player_1'];
    players.forEach(playerId => {
        zones.forEach(zone => {
            initialState[playerId][zone].forEach(cardId => {
                cards[cardId].location = {playerId, zone};
            });
        });
    });
    return initialState;
}

function triggerEvent(currentState, ctx, eventId, payload) {
    // First of all, iterate through all cards in the current player's
    // field to see if they respond.
    const helper = new GameHelper(currentState, ctx);
    let {currentPlayer} = helper.getCurrentPlayer();
    let {opponentPlayer} = helper.getOpponentPlayer();
    // Collect cards from current player's field and opponent's field.
    const activeCardIds = [...currentPlayer.field, ...opponentPlayer.field];
    // Find all valid triggers in the player's field.
    let triggers = activeCardIds.map(cardId => {
        let card = currentState.cards[cardId];
        return card.proto.routines.filter(routine => {
            return routine.type === "triggered" && routine.event.id === eventId;
        }).map(routine => {
            return {...routine, cardId};
        });
    });
    // Flatten the triggers array. Notice how the above map function returns
    // nested arrays: triggers = [[...], [...], [...]]
    // The .reduce function will flatten this to: [..., ..., ...]
    triggers = triggers.reduce((arr, triggers) => arr.concat(triggers), []);
    // Filter out trigger that don't match it's event parameters.
    triggers = triggers.filter(trigger => {
        return GameEventValidator[eventId](state, ctx, trigger, payload);
    });
    // Execute each response.
    let state = currentState;
    triggers.forEach(trigger => {
        trigger.response.forEach(response => {
            let resultState = GameEvents[response.id](state, ctx, GameLogic, trigger, response);
            if (resultState) {
                state = resultState;
            }
        });
    })
    return state;
}

function drawCard(currentState, ctx) {
    let playerId = "player_" + ctx.currentPlayer;
    return drawCardForPlayer(currentState, ctx, playerId);
}

function drawCardForPlayer(currentState, ctx, playerId) {
    let helper = new GameHelper(currentState, ctx);
    let player = currentState[playerId];
    // Add the last card in the player's deck to their hand.
    let deckIndex = player.deck.length - 1;
    let cardId = player.deck[deckIndex];
    let hand = ImmutableArray.append(player.hand, cardId);
    helper.updateCardLocation(cardId, {playerId, zone: 'hand'});
    // Remove the last card in the deck.
    let deck = ImmutableArray.removeAt(player.deck, deckIndex);
    // Construct and return a new state object with our changes.
    let state = helper.constructStateForPlayer(playerId, {hand, deck});
    return triggerEvent(state, ctx, 'draw-card', {cardId, playerId});
}

function playCard(currentState, ctx, cardId) {
    let helper = new GameHelper(currentState, ctx);
    let {currentPlayer, playerId} = helper.getCurrentPlayer();
    // Find the card in their hand.
    let handIndex = currentPlayer.hand.indexOf(cardId);
    let card = currentState.cards[cardId];
    // Ensure the card is in the player's hand and they can afford it.
    if (handIndex !== -1 
        && card 
        && currentPlayer.cpu >= card.proto.cpu_cost 
        && currentPlayer.memory >= card.proto.memory_cost) {
        // Add the card to the player's field.
        let field = ImmutableArray.append(currentPlayer.field, currentPlayer.hand[handIndex]);
        helper.updateCardLocation(currentPlayer.hand[handIndex], {playerId, zone: 'field'});
        // Remove the card from their hand.
        let hand = ImmutableArray.removeAt(currentPlayer.hand, handIndex);
        // Pay the CPU cost.
        let cpu = currentPlayer.cpu - card.proto.cpu_cost;
        // Pay the Memory cost.
        let memory = currentPlayer.memory - card.proto.memory_cost;
        // Construct and return a new state object with our changes.
        let state = helper.constructStateForPlayer(playerId, {hand, field, cpu, memory})
        return triggerEvent(state, ctx, 'play-card', {playerId, cardId, category: card.proto.category});
    } else {
        // We return the unchanged state if we can't play a card.
        return currentState;
    }
}

function trashCard(currentState, ctx, cardId) {
    const helper = new GameHelper(currentState, ctx);
    const card = currentState.cards[cardId];
    const playerId = card.location.playerId;
    const player = currentState[playerId];
    const currentZoneId = card.location.zone;
    const currentZone = player[currentZoneId];
    const isCardValid = card && currentZone.includes(cardId);
    if (isCardValid) {
        // Add the card to the player's trash.
        const trash = ImmutableArray.append(player.trash, cardId);
        helper.updateCardLocation(cardId, {playerId, zone: 'trash'});
        // Remove the card from it's current location.
        let currentZoneIndex = currentZone.indexOf(cardId);
        const zone = ImmutableArray.removeAt(currentZone, currentZoneIndex);
        return helper.constructStateForPlayer(playerId, {trash, [currentZoneId]: zone});
    }
    return currentState;
}

function dealDamage(currentState, ctx, cardId, damage) {
    let state = currentState;
    const { getProp } = GameHelper;
    const currentCard = state.cards[cardId];
    const strength = getProp(currentCard, "strength") - damage;
    const card = {...currentCard, strength};
    const cards = ImmutableArray.set(state.cards, card, cardId);
    // Trash if card destroyed.
    if (strength <= 0) {
        state = trashCard(state, ctx, cardId);
    }
    // Notice how we call triggerEvent(). This dealDamage() was called from triggerEvent.
    // The recursion leads to complex behaviour.
    state = triggerEvent(state, ctx, 'deal-damage', {cardId});
    return {...state, cards};
}

function attack(currentState, ctx, instigatorId, routineIndex, targetId) {
    let helper = new GameHelper(currentState, ctx);
    let { getRoutineProp, getProp } = GameHelper;
    let { currentPlayer, playerId } = helper.getCurrentPlayer();
    let { opponentPlayer, opponentPlayerId } = helper.getOpponentPlayer();
    // Get the card that instigates the attack, and the attack target from the current state.
    let instigator = currentState.cards[instigatorId];
    let target = currentState.cards[targetId];
    // Check that the cards are valid and in the correct zones.
    let areCardsValid = instigator
        && target
        && currentPlayer.field.includes(instigatorId)
        && opponentPlayer.field.includes(targetId)
    if (areCardsValid) {
        let attack = instigator.proto.routines[routineIndex];
        // Check if the player can afford the cpu cost of the attack and the attack has not already
        // been used this turn.
        let didUseAttack = (instigator.usedRoutines && instigator.usedRoutines.includes(routineIndex));
        let canAttack = !didUseAttack && attack.type === "attack" && instigator.booted && currentPlayer.cpu >= attack.cpu_cost;
        if (canAttack) {
            // Pay the CPU cost.
            const cpu = currentPlayer.cpu - getRoutineProp(instigator, routineIndex, 'cpu_cost');
            const damage = getRoutineProp(instigator, routineIndex, 'damage');
            helper.updateState(dealDamage(helper.state, ctx, targetId, damage));
            // 'Use' up the attack for this turn.
            const usedRoutines = [...(instigator.usedRoutines || []), routineIndex];
            // Return the new state object.
            const cards = ImmutableArray.set(helper.state.cards, {...instigator, usedRoutines}, instigatorId);
            return {...helper.constructStateForPlayer(playerId, {cpu}), cards};
        }
    }
    return currentState;
}

function onTurnStart(currentState, ctx) {
    let helper = new GameHelper(currentState, ctx);
    let {currentPlayer, playerId} = helper.getCurrentPlayer();
    // Increment and restore the player's CPU.
    let maxCpu = currentPlayer.maxCpu + 1;
    // Iterate through all cards on the player's field.
    let cardUpdates = currentPlayer.field.map(cardId => {
        let currentCard = currentState.cards[cardId];
        // Reset card strength, clear usedAttacks and finish booting.
        let card = {
            ...currentCard, 
            usedRoutines: [],
            strength: currentCard.proto.strength,
            booted: true, 
        };
        return {index: cardId, value: card};
    });
    // Create a new cards array, with the updated cards.
    let cards = ImmutableArray.multiSet(currentState.cards, cardUpdates);
    let state = {...helper.constructStateForPlayer(playerId, {maxCpu, cpu: maxCpu}), cards};
    // Draw a card.
    return drawCard(state, ctx);
}

const GameLogic = {
    initialState, 
    drawCardForPlayer,
    drawCard,
    playCard, 
    trashCard, 
    attack,
    dealDamage,
    onTurnStart
};
export default GameLogic;