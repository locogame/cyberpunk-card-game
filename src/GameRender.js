import React from 'react';
import {GameHelper} from './GameHelper';

function Attack(props) {
    const {card, index} = props;
    const {getRoutineProp} = GameHelper;
    const cpuCost = getRoutineProp(card, index, 'cpu_cost');
    const damage = getRoutineProp(card, index, 'damage');
    const isDisabled = props.team === "enemy" || (card.usedRoutines && card.usedRoutines.includes(index)) || !card.booted;
    return <div>
        <button onClick={() => props.onClick(card.id, index)} disabled={isDisabled}>{cpuCost} CPU: Deal {damage} damage.</button>
    </div>;
}

function TriggeredRoutine(props) {
    return <pre className="routine">{props.routine.text}</pre>;
}

function Card(props) {
    const {id, proto, booted} = props.card;
    const {getProp} = GameHelper;
    const cpuCost = getProp(props.card, 'cpu_cost');
    const memoryCost = getProp(props.card, 'memory_cost');
    const strength = getProp(props.card, 'strength');
    const additionalButtons = [];
    const attacks = proto.routines.filter(r => r.type === "attack").map((attack, index) => {
        let onAttack = () => {
            props.onAttack(index);
        };
        return <Attack key={index} card={props.card} team={props.team} attack={attack} index={index} onClick={onAttack} />
    });
    const triggeredRoutines = proto.routines.filter(r => r.type === "triggered").map((routine, index) => {
        return <TriggeredRoutine key={index} routine={routine} />;
    });
    if (props.attacking && props.zone !== 'hand') {
        additionalButtons.push(<button key="cancel-attack" onClick={props.onCancelAttack}>Cancel Attack</button>);
    }
    if (props.targeting && props.zone !== 'hand') {
        additionalButtons.push(<button key="target" onClick={props.onTargeted}>Target</button>);
    }
    if (props.zone === 'hand') {
        additionalButtons.push(<button key="play" onClick={props.onPlay}>Play</button>);
    }
    return <div className={`card card-${proto.category} card-${booted ? 'booted' : 'unbooted'}`}>
        { booted || props.zone === 'hand' ? null : 'booting...'}
        <p>{proto.title} [#{id}]</p>
        <div class="stats">
            <Stat icon="processor" value={cpuCost} />
            <Stat icon="ram" value={memoryCost} />
            <Stat icon="heart-beats" value={strength} />
        </div>
        {attacks}
        {triggeredRoutines}
        {additionalButtons}
    </div>;
}

function HiddenCard(props) {
    return <div className="card card-hidden"></div>
}

class Stat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            statClass: 'stat'
        };
    }

    componentDidUpdate(prevProps) {
        if (prevProps.value !== this.props.value) {
            this.setState({
                statClass: 'stat highlight'
            });
            setTimeout(() => {
                this.setState({
                    statClass: 'stat'
                });
            }, 2000);
        }
    }

    render() {
        let {icon, value} = this.props;
        let statClass = []
        return <span>
            <img src={"/icon/"+icon+".png"} width="32" className="icon"/>
            <span className={this.state.statClass}>{value}</span>
        </span>;
    }
}

// This is a React component.
// If you've not used React before I recommend you read up on it.
// TODO: Change the Renderer to use three.js
class GameRender extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.prepareAttack = this.prepareAttack.bind(this);
        this.attack = this.attack.bind(this);
        this.cancelAttack = this.cancelAttack.bind(this);
        this.playCard = this.playCard.bind(this);
    }

    prepareAttack(instigatorId, attackIndex) {
        this.setState({
            currentAttack: {instigatorId, attackIndex},
        });
    }

    attack(instigatorId, attackIndex, targetId) {
        this.props.moves.attack(instigatorId, attackIndex, targetId);
        this.setState({currentAttack: null});
    }

    playCard(cardId) {
        this.props.moves.playCard(cardId);
    }

    cancelAttack() {
        this.setState({currentAttack: null});
    }

    // Split the card rendering into it's own function.
    // I've also added buttons for the attacks.
    renderCard(cardId, zone, team) {
        let card = this.props.G.cards[cardId];
        let onPlay = () => {
            this.playCard(cardId);
        };
        let onAttack = (attackIndex) => {
            this.prepareAttack(cardId, attackIndex);
        };
        let onTargeted = () => {
            this.attack(this.state.currentAttack.instigatorId, this.state.currentAttack.attackIndex, cardId);
        };
        return <Card 
            key={cardId} 
            card={card} 
            zone={zone} 
            team={team}
            attacking={this.state.currentAttack && this.state.currentAttack.instigatorId === cardId}
            targeting={this.state.currentAttack && this.state.currentAttack.instigatorId !== cardId}
            onPlay={onPlay} 
            onAttack={onAttack} 
            onCancelAttack={this.cancelAttack} 
            onTargeted={onTargeted} />
    }

    renderHiddenCard(index, team) {
        return <HiddenCard key={index} team={team} />
    }

    render() {
        // Get state references.
        const state = this.props.G;
        const ctx = this.props.ctx;
        const helper = this.helper = new GameHelper(state, ctx);
        const {currentPlayer, playerId} = helper.getCurrentPlayer();
        const {opponentPlayer, opponentPlayerId} = helper.getOpponentPlayer();
        
        // Create an array of <div> for each card in the player hand.
        // React allows JSX. I can combine HTML and Javascript in the same file.
        const playerHand = currentPlayer.hand.map(c => this.renderCard(c, 'hand', 'friendly'));
        const playerField = currentPlayer.field.map(c => this.renderCard(c, 'field', 'friendly'));
        const opponentField = opponentPlayer.field.map(c => this.renderCard(c, 'field', 'enemy'));
        const opponentHand = opponentPlayer.hand.map(c => this.renderHiddenCard(c, 'hand', 'enemy'));
        return <div>
            <div className="board">
                <div id="hand-player">
                    <h4>{playerId}</h4>
                    <div className="stats">
                        <Stat icon="processor" value={currentPlayer.cpu} />
                        <Stat icon="ram" value={currentPlayer.memory} />
                    </div>
                    {playerHand}
                </div>
                <div id="field-player">{playerField}</div>
                <div id="field-opponent">{opponentField}</div>
                <div id="hand-opponent">
                    <h4>{opponentPlayerId}</h4>
                    <div className="stats">
                        <Stat icon="processor" value={opponentPlayer.cpu} />
                        <Stat icon="ram" value={opponentPlayer.memory} />
                    </div>
                    {opponentHand}
                </div>
            </div>
        </div>;
    }
}

export default GameRender;